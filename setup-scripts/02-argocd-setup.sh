#!/bin/bash

set -euo pipefail
# kubectl create namespace argocd

helm repo add argo https://argoproj.github.io/argo-helm
helm install argo-cd argo/argo-cd --namespace argocd --create-namespace
