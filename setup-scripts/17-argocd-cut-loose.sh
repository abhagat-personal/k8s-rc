#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

kubectl delete -n argocd secret argocd-initial-admin-secret

echo "Congrats! We have now deleted the secret that generated the initial admin"
echo "user for argoCD. Note that we can still log in with the admin user - this password"
echo "is stored as a bcrypt hash in argocd-secret. You can decide whether or not"
echo "you'd like to delete this account entirely on your own. Please consult the docs"
echo "and figure out what works best for you!"