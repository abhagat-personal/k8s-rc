#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${GITLAB_PAT}failcheck"

for deploy in "dex-server" "redis" "repo-server" "server"; \
  do kubectl -n argocd rollout status deploy/argo-cd-argocd-${deploy}; \
done

port_forwarding_cmd="kubectl -n argocd port-forward svc/argo-cd-argocd-server 8080:443"

echo "When this command completes, you should now familiarize yourself with port forwarding to ArgoCD. You can do this by opening another terminal tab and typing:"
echo ""
echo "$port_forwarding_cmd"
echo 
echo "Once you have forwarding set up, you should be able to navigate to localhost:8080 and log into argocd as admin. The password can be retrieved by running:"
echo ""
echo 'kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d'
echo ""
echo "ArgoCD will be the system of record for the deployment state - so keep and eye on what's happening in the panel as these scripts are run."
echo ""
echo "If something goes wrong with port forwarding, you can use CTRL+C  and use again use port fowarding to re-establish the connection."
echo ""
echo "The next script will assume that you have port forwarding running in a different tab, as will all subsequent scripts until ArgoCD OIDC is set up."
