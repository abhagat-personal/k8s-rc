kubectl -n argocd delete deployment argo-cd-argocd-application-controller
kubectl -n argocd delete deployment argo-cd-argocd-dex-server
kubectl -n argocd delete deployment argo-cd-argocd-redis
kubectl -n argocd delete deployment argo-cd-argocd-repo-server
kubectl -n argocd delete deployment argo-cd-argocd-server

kubectl -n argocd delete service argo-cd-argocd-application-controller
kubectl -n argocd delete service argo-cd-argocd-dex-server
kubectl -n argocd delete service argo-cd-argocd-redis
kubectl -n argocd delete service argo-cd-argocd-repo-server
kubectl -n argocd delete service argo-cd-argocd-server

kubectl delete clusterrolebinding argo-cd-argocd-application-controller
kubectl delete clusterrolebinding argo-cd-argocd-server
kubectl delete clusterrole argo-cd-argocd-application-controller       
kubectl delete clusterrole argo-cd-argocd-server  

echo "Your initial argoCD setup should be deleted, and the setup will now self-manage."
