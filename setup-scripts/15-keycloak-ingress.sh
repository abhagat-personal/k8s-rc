#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${DOMAIN_NAME}failcheck"

cat << EOF > keycloak-ingress.yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: keycloak-ingress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-staging"
    nginx.ingress.kubernetes.io/configuration-snippet: |
      proxy_set_header l5d-dst-override sso-keycloak.keycloak.svc.cluster.local:8080;
      grpc_set_header l5d-dst-override sso-keycloak.keycloak.svc.cluster.local:8080;
spec:
  tls:
  - hosts:
    - auth.$DOMAIN_NAME
    secretName: auth-tls
  rules:
  - host: auth.$DOMAIN_NAME
    http:
      paths:
      - backend:
          serviceName: sso-keycloak
          servicePort: 80
EOF

cat << EOF > ingress-nginx.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: ingress-nginx
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: k8s-core/ingress-nginx
  destination:
    server: https://kubernetes.default.svc
    namespace: ingress-nginx
  syncPolicy:
    automated: {}
    syncOptions:
      - CreateNamespace=true
EOF

cat << EOF > Chart.yaml
apiVersion: v2
name: ingress-nginx
description: Kubernetes-supported NGINX controller
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.0.1

dependencies:
- name: ingress-nginx
  version: 3.27.0
  repository: https://kubernetes.github.io/ingress-nginx
EOF

cat << EOF > values.yaml
ingress-nginx:
  controller:
    service:
      annotations:
        nginx.ingress.kubernetes.io/enable-modsecurity: "true"
        nginx.ingress.kubernetes.io/enable-owasp-core-rules: "true"
        external-dns.alpha.kubernetes.io/hostname: $DOMAIN_NAME.
    podAnnotations:
      linkerd.io/inject: ingress
    deploymentAnnotations:
      linkerd.io/inject: enabled
    config:
      enable-modsecurity: enabled
      enable-owasp-modsecurity-crs: enabled
      enable-ocsp: enabled
EOF

mv ingress-nginx.yaml k8s-core
mv keycloak-ingress.yaml auth/sso/templates

mkdir k8s-core/ingress-nginx
mv Chart.yaml k8s-core/ingress-nginx
mv values.yaml k8s-core/ingress-nginx

git add auth
git add k8s-core
git commit -m "add http service and ingress for keycloak"

git push origin main
argocd app sync k8s-core-apps

echo "This is the end of the mostly-automated steps. At this point, we'll have to deal"
echo "with keycloak, which can be its own beast to configure and set up."

echo ""

echo "You should soon (10 or so minutes) be able to log into auth.$DOMAIN_NAME to administer keycloak. Keep in mind that DNS propagation may take a while on top of this."

echo 'To obtain the Keycloak administration credentials for your cluster, you should run:'
echo "kubectl -n keycloak get secret credential-sso -o go-template=""'"'{{range $k,$v := .data}}{{printf "%s: " $k}}{{if not $v}}{{$v}}{{else}}{{$v | base64decode}}{{end}}{{"\n"}}{{end}}'"'"

echo "Please follow the guide at: https://argoproj.github.io/argo-cd/operator-manual/user-management/keycloak/"
echo "and configure your own realm (with the realm name you indicated in the .env file), client (named argocd) and user"
echo "You can stop before the portion of the guide that asks you to base64-encode the OIDC secret. Copy the OIDC secret and use it as an argument to the next script."
