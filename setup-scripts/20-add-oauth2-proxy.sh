#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

oauth2_proxy_keycloak_client_secret=$1

echo -n "If you're running this script, you'll need to have made a Keycloak client for Oauth2-proxy."
echo -n "This script will fail without a secret provided"

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${DOMAIN_NAME}failcheck"
failcheck="${KEYCLOAK_REALM}failcheck"
failcheck="${oauth2_proxy_keycloak_client_secret}failcheck"

base64_client_secret=$(echo -n $oauth2_proxy_keycloak_client_secret | base64) 
base64_client_id=$(echo -n "oauth2-proxy" | base64)
base64_server_secret=$(openssl rand -base64 32 | head -c 32 | base64)

cat << EOF > oauth2-proxy-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  creationTimestamp: null
  name: oauth2-proxy-secret
  namespace: oauth2-proxy
data:
  cookie-secret: $base64_server_secret
  client-secret: $base64_client_secret
  client-id: $base64_client_id
EOF

kubeseal --format=yaml < oauth2-proxy-secret.yaml> sealed-oauth2-proxy-secret.yaml
rm oauth2-proxy-secret.yaml

cat << EOF > Chart.yaml
apiVersion: v2
name: oauth2-proxy
description: A Helm chart for Kubernetes
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.0.1

dependencies:
- name: oauth2-proxy
  version: 5.0.4
  repository: https://k8s-at-home.com/charts/
EOF

cat << EOF > values.yaml
oauth2-proxy:
  config:
    existingSecret: oauth2-proxy-secret
    configFile: |-
      provider = "oidc"
      provider_display_name = "$KEYCLOAK_REALM"
      oidc_issuer_url = "https://auth.$DOMAIN_NAME/auth/realms/$KEYCLOAK_REALM"
      scope = "openid profile email groups"
      email_domains = [ "*" ]
      cookie_domains = ".$DOMAIN_NAME"
      whitelist_domains = ".$DOMAIN_NAME"
      pass_authorization_header = true
      pass_access_token = true
      pass_user_headers = true
      set_authorization_header = true
      set_xauthrequest = true
      cookie_refresh = "1m"
      cookie_expire = "30m"
      reverse_proxy = true
      ssl_insecure_skip_verify = true
  # redis:
  #   enabled: true
  #   cluster:
  #     enabled: false
  #     slaveCount: 1 # sigh
  #   usePassword: false
  #   redisPort: 6379
EOF

cat << EOF > oauth2-proxy.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: oauth2-proxy
  annotations:
    linkerd.io/inject: enabled
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: oauth2-proxy
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: auth/oauth2-proxy
  destination:
    server: https://kubernetes.default.svc
    namespace: oauth2-proxy
  syncPolicy:
    automated:
      selfHeal: true  
EOF

cat << EOF > oauth2-proxy-ingress.yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: oauth2-proxy-ingress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-staging"
    nginx.ingress.kubernetes.io/proxy-buffer-size: "16k"
    nginx.ingress.kubernetes.io/configuration-snippet: |
      proxy_set_header l5d-dst-override oauth2-proxy.oauth2-proxy.svc.cluster.local:80;
spec:
  tls:
  - hosts:
    - oauth2.$DOMAIN_NAME
    secretName: oauth2-tls
  rules:
  - host: oauth2.$DOMAIN_NAME
    http:
      paths:
      - backend:
          serviceName: oauth2-proxy
          servicePort: 80
EOF

mkdir auth/oauth2-proxy
mv oauth2-proxy.yaml auth

mkdir auth/oauth2-proxy/templates
mv Chart.yaml auth/oauth2-proxy
mv values.yaml auth/oauth2-proxy
mv sealed-oauth2-proxy-secret.yaml auth/oauth2-proxy/templates
mv oauth2-proxy-ingress.yaml auth/oauth2-proxy/templates

git add auth
git commit -m "add oauth2 proxy support for group-restriction of endpoints"
git push origin main
