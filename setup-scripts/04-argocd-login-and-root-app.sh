#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${GITLAB_PAT}failcheck"

for deploy in "dex-server" "redis" "repo-server" "server"; \
  do kubectl -n argocd rollout status deploy/argo-cd-argocd-${deploy}; \
done

argo_pass=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
argocd login localhost:8080 --username admin --password $argo_pass

# TODO: make sure this is removed later
argocd repo add $GITLAB_REPO_URL --username any-non-empty-string --password $GITLAB_PAT

mkdir apps

cat << EOF > root.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: root
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: apps
  destination:
    server: https://kubernetes.default.svc
    namespace: argocd
  syncPolicy:
    automated:
      selfHeal: true  
EOF

mv root.yaml apps

git add apps
git commit -m "add argoCD root application"
git push origin main

kubectl apply -f apps
argocd app sync root
