#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"

mkdir argocd

cat << EOF > argocd.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: argocd
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: argocd
  destination:
    server: https://kubernetes.default.svc
    namespace: argocd
  syncPolicy:
    automated:
      selfHeal: true  
EOF

mv argocd.yaml apps

cat << EOF > Chart.yaml
apiVersion: v2
name: argocd
description: A Helm chart for Kubernetes
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.0.1

dependencies:
- name: argo-cd
  version: 3.6.4
  repository: https://argoproj.github.io/argo-helm
EOF

cat << EOF > values.yaml
argo-cd:
  installCRDs: false
EOF

mv Chart.yaml argocd
mv values.yaml argocd

git add apps
git add argocd
git commit -m "add ability for argoCD to self-manage"
git push origin main

argocd app sync root


echo "At this point, you will have two versions of ArgoCD running in your cluster - your initial installation, and, separately, a self-managed helm copy."
echo ""
echo "You should navigate to the terminal window where you were port forwarding to ArgoCD and use CTRL+C to exit the process."
echo ""
echo "Replace the port forwarding you were just doing with a similar command:"
echo ""
echo 'kubectl -n argocd port-forward svc/argocd-server 8080:443'
echo ""
echo "When port forwarding has resumed using the above command, you can proceed to the next step, which will clean up the initial install."
